package com.example.contentprovider.contact.getcontact;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;

import com.example.contentprovider.contact.interfaces.ContactEventListener;
import com.example.contentprovider.model.GetContactModel;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

public class GetContactController extends Thread {

    private Context context;
    private ContactEventListener eventListener;
    private Map<Event, Object> map;

    public GetContactController(Context context, ContactEventListener eventListener) {
        this.context = context;
        this.eventListener = eventListener;
    }

    @Override
    public void run() {
        super.run();
        try {
            sendOnUIThread(Event.IN_PROGRESS, null);
            List<GetContactModel> data = getPhoneContactData();
            sendOnUIThread(Event.COMPLETE, data);
        } catch (Exception e) {
            sendOnUIThread(Event.ERROR, e);
        }
    }

    private void sendOnUIThread(Event key, Object value) {
        map = new HashMap<>();
        Message message = null;
        if (key.name().equals(Event.COMPLETE.name())) {
            map.put(Event.COMPLETE, value);
            message = handler.obtainMessage(0, map);
            message.sendToTarget();
        } else if (key.name().equals(Event.ERROR.name())) {
            map.put(Event.ERROR, null);
            message = handler.obtainMessage(0, map);
            message.sendToTarget();
        } else if (key.name().equals(Event.IN_PROGRESS.name())) {
            map.put(Event.IN_PROGRESS, null);
            message = handler.obtainMessage(0, map);
            message.sendToTarget();
        }
    }

    public List<GetContactModel> getPhoneContactData() {
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null)
            throw new NullPointerException("Content resolver class is required");

        List<GetContactModel> list = new ArrayList<>();


        Cursor cursor = contentResolver
                .query(ContactsContract.Contacts.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);
        if (cursor == null)
            throw new NullPointerException("Cursor class is required");

        if (cursor.moveToFirst()) {
            do {
                GetContactModel getContactModel = new GetContactModel();
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String contactPhoto = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
                // set corresponding data to get contact model without contact number
                getContactModel.setImage(contactPhoto);
                getContactModel.setName(contactName);
                getContactModel.setId(id);
                List<String> contactNumberList = new ArrayList<>();

                Cursor phones = contentResolver
                        .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " =?",
                                new String[]{id},
                                null);

                if (phones.moveToFirst()) {
                    do {
                        String contactNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        contactNumberList.add(contactNumber);
                    } while (phones.moveToNext());
                }
                // combine get contact data
                getContactModel.setContactNumber(contactNumberList);
                list.add(getContactModel);
            } while (cursor.moveToNext());
        }
        return  list;
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(@NonNull Message msg) {
            // run on UI thread
            try {
                handleEventsOnUIThread(msg);
            } catch (ClassCastException e) {
                eventListener.onError(e);
            }
        }
    };

    private void handleEventsOnUIThread(Message msg) {
        HashMap<Event, Object> map = (HashMap<Event, Object>) msg.obj;
        String key = map
                .keySet()
                .iterator()
                .next()
                .name();

        Object value = map
                .entrySet()
                .iterator()
                .next()
                .getValue();

        if (Event.COMPLETE.name().equals(key)) {
            List<GetContactModel> models = (List<GetContactModel>) value;
            eventListener.onCompleted(models);
        } else if (Event.ERROR.name().equals(key)) {
            Exception error = (Exception) value;
            eventListener.onError(error);
        } else if (Event.IN_PROGRESS.name().equals(key)) {
            eventListener.inProgress();
        }
    }

    enum Event {
        ERROR, COMPLETE, IN_PROGRESS
    }
}
