package com.example.contentprovider.contact.interfaces;

public interface ContactConnection {
    void getContact();
    void addListener(ContactEventListener eventListener);
}
