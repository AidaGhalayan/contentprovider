package com.example.contentprovider.contact.interfaces;

import com.example.contentprovider.model.GetContactModel;

import java.util.List;

public interface ContactEventListener {
    void onError(Exception error);
    void inProgress();
    void onCompleted(List<GetContactModel> contacts);
}
