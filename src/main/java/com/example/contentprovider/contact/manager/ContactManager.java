package com.example.contentprovider.contact.manager;

import android.content.Context;

import com.example.contentprovider.contact.getcontact.GetContactController;
import com.example.contentprovider.contact.interfaces.ContactConnection;
import com.example.contentprovider.contact.interfaces.ContactEventListener;

public class ContactManager implements ContactConnection {
    private Context context;
    private ContactEventListener eventListener;

    public ContactManager(Context context) {
        this.context = context;
    }

    @Override
    public void getContact() {
        GetContactController controller = new GetContactController(context, eventListener);
        controller.start();
    }

    @Override
    public void addListener(ContactEventListener eventListener) {
        this.eventListener = eventListener;
    }
}
