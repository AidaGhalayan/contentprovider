package com.example.contentprovider.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.contentprovider.R;
import com.example.contentprovider.adapter.RecyclerViewAdapter;
import com.example.contentprovider.contact.getcontact.GetContactController;
import com.example.contentprovider.contact.interfaces.ContactConnection;
import com.example.contentprovider.contact.interfaces.ContactEventListener;
import com.example.contentprovider.contact.manager.ContactManager;
import com.example.contentprovider.fragments.RecViewFragment;
import com.example.contentprovider.model.GetContactModel;

import java.util.List;

public class MainActivity extends AppCompatActivity implements
        ActivityCompat.OnRequestPermissionsResultCallback, ContactEventListener {

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 100;
    private ContactConnection contactConnection;
    private ProgressBar progressBar;

    private List<GetContactModel> getContactModels;

    private RecyclerView recyclerView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getPermissions();


        getFragment();


        contactConnection = new ContactManager(this);
        contactConnection.addListener(this);
        contactConnection.getContact();
    }

    private void getPermissions() {
        ActivityCompat.requestPermissions(MainActivity.this,
                new String[]{Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS},
                LOCATION_PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // start to find location...
            } else { // if permission is not granted
                Toast.makeText(MainActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                // decide what you want to do if you don't get permissions
            }

        }
    }

    @Override
    public void onError(Exception error) {
        System.out.println();
    }

    @Override
    public void inProgress() {
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCompleted(List<GetContactModel> contacts) {
        this.getContactModels = contacts;
        ((RecViewFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_container_rec_view)).setContacts(contacts);
        progressBar.setVisibility(View.INVISIBLE);
    }

    private void getFragment() {
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container_rec_view, new RecViewFragment())
                .commit();

    }
}
