package com.example.contentprovider.model;


import java.util.List;

public class GetContactModel {
    private String id;
    private List<String> contactNumbers;
    private String name;
    private String image;

    public GetContactModel() {
    }

    public GetContactModel(List<String> contactNumber, String name, String image) {
        this.contactNumbers = contactNumber;
        this.name = name;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<String> getContactNumber() {
        return contactNumbers;
    }

    public void setContactNumber(List<String> contactNumber) {
        this.contactNumbers = contactNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public StringBuilder getNumbers() {
        StringBuilder stringBuilder = new StringBuilder();
        for (String number : contactNumbers) {
            stringBuilder.append(number).append("\n");
        }
        return stringBuilder;
    }
    @Override
    public String toString() {
        return "GetContactModel{" +
                "contactNumber='" + contactNumbers.toString() + '\'' +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
