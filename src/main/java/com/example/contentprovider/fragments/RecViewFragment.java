package com.example.contentprovider.fragments;


import android.content.Context;
import android.media.Image;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.contentprovider.R;
import com.example.contentprovider.adapter.RecyclerViewAdapter;
import com.example.contentprovider.model.GetContactModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class RecViewFragment extends Fragment {

    private Context context;
    List<GetContactModel> getContactModels = new ArrayList<>();

    private RecyclerView recyclerView;
    private RecyclerViewAdapter adapter;

    public RecViewFragment() {
        configureRecyclerView(getContactModels);
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        return inflater.inflate(R.layout.fragment_rec_view, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_view);
        configureRecyclerView(getContactModels);
    }

    private void configureRecyclerView(List<GetContactModel> getContactModels) {
        if (recyclerView != null) {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()), RecyclerView.VERTICAL));
            adapter = new RecyclerViewAdapter(getContactModels);
            recyclerView.setAdapter(adapter);
        }

    }
    public void setContacts(List<GetContactModel> getContactModels){
        adapter.updateData(getContactModels);
        adapter.notifyDataSetChanged();



    }
}
