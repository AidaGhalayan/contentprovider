package com.example.contentprovider.fragments;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.contentprovider.R;
import com.example.contentprovider.model.GetContactModel;

/**
 * A simple {@link Fragment} subclass.
 */
public class DialogFragmentForDial extends DialogFragment {
    private Context context;

    private Button firstButton;
    private Button secondButton;
    private Button thirdButton;



    public DialogFragmentForDial(Context context) {
        this.context=context;
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dialog_fragment_for_dial, container, false);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        if(getActivity()!=null){
            setCancelable(false);
            View view=getActivity().getLayoutInflater().inflate(R.layout.fragment_dialog_fragment_for_dial, null);
            firstButton= view.findViewById(R.id.first_button);

            firstButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "", Toast.LENGTH_SHORT).show();


                }
            });
            secondButton= view.findViewById(R.id.second_button);
            firstButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, "", Toast.LENGTH_SHORT).show();


                }
            });
            return new AlertDialog.Builder(getActivity())
                    .setView(view)
                    .create();
        }
        return super.onCreateDialog(savedInstanceState);
}


}
