package com.example.contentprovider.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contentprovider.R;
import com.example.contentprovider.model.GetContactModel;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static com.example.contentprovider.R.drawable.ic_account_box_black_24dp;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder> {
    private List<GetContactModel> contactModels;
    private Context context;


    public RecyclerViewAdapter(List<GetContactModel> getContactModels) {
        this.contactModels = getContactModels;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        MyViewHolder myViewHolder = null;
        switch (getItemViewType(viewType)){
            case 0:
                myViewHolder= new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item,
                        null, false));
                break;
            case  1:
                myViewHolder= new MyViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.rec_veiw_type_item,
                        null, false));
                break;
            
        }return myViewHolder;

        
    }

    public void updateData(List<GetContactModel> viewModels) {
        contactModels.clear();
        contactModels.addAll(viewModels);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.setPosition(position);
        
        GetContactModel contactModel = contactModels.get(position);
        holder.bind(contactModel);
    }

    @Override
    public int getItemCount() {
        return contactModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView contactPhoto;
        private TextView contactName;
        private TextView contactNumber;

        private int position;
        public void setPosition(int position){
            this.position=position;
        }

        public MyViewHolder(@NonNull final View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (contactModels.get(position).getContactNumber().size() > 1) {


                        Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
                    } else {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + contactModels.get(position).getContactNumber().get(0)));
                        context.startActivity(intent);

                    }
                }
            });
            contactPhoto = itemView.findViewById(R.id.profile_image);
            contactName = itemView.findViewById(R.id.contact_name);
            contactNumber = itemView.findViewById(R.id.phone_number);
        }

        private void bind(GetContactModel item) {
            if (item.getImage() != null) {
                contactPhoto.setImageURI(Uri.parse(item.getImage()));
            }
            else {
                contactPhoto.setImageResource(ic_account_box_black_24dp);

            }
            if (item.getContactNumber() != null && item.getContactNumber().size() >= 1) {
                contactNumber.setText(item.getContactNumber().toString());
            }
            if (item.getName() != null) {
                contactName.setText(item.getName());
            }
        }
    }
}
