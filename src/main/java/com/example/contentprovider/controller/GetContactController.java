package com.example.contentprovider.controller;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.util.Log;

import com.example.contentprovider.model.GetContactModel;

import java.util.ArrayList;
import java.util.List;

public class GetContactController {

    private Context context;

    public GetContactController(Context context) {
        this.context = context;
    }

    public List<GetContactModel> getPhoneContactData() {
        ContentResolver contentResolver = context.getContentResolver();
        if (contentResolver == null)
            throw new NullPointerException("Content resolver class is required");

        Cursor cursor = contentResolver
                .query(ContactsContract.Contacts.CONTENT_URI,
                        null,
                        null,
                        null,
                        null);
        if (cursor == null)
            throw new NullPointerException("Cursor class is required");
        List<GetContactModel> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                String image = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));



                GetContactModel model = new GetContactModel(null, name, image);
                list.add(model);
                Log.i("taggggg", "getPhoneContactData: " + model.toString());
            } while (cursor.moveToNext());
        }

        return list;
    }

}
